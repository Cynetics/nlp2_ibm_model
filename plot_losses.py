import matplotlib.pyplot as plt
plt.style.use('ggplot')
plt.rcParams["axes.axisbelow"] = True

class Plot(object):
  def __init__(self,train_loss, val_loss):
    self.train_loss = train_loss
    self.val_loss = val_loss

  def plot(self):
    print("Plotting the Losses")
    plt.plot(self.train_loss, label="Train Loss")
    plt.plot(self.val_loss, label="Validation Loss")
    plt.ylabel("Negative Log Loss")
    plt.xlabel("Iterations")
    plt.grid()
    plt.legend()
    plt.title("Loss vs Time")
    plt.show()
