import numpy as np
import itertools
from collections import defaultdict
import time


class IBM1(object):
    def __init__(self, data_reader, train_sentences, test_sentences, english_lex, french_lex, N):
        self.train_sentences = train_sentences
        self.data_reader = data_reader
        self.n_train_sentences = len(train_sentences[0])
        self.english_lex = english_lex
        self.french_lex = french_lex
        self.test_sentences = test_sentences
        self.N = N
        self.word_alignments = self.data_reader.create_alignments(self.train_sentences, self.N)

    def EM(self):
      probability_dictionary = self.initialize_dictionary()
      iteration_counter = 0
      evaluations_val = []
      evaluations_train = []
      for i in range(10):

          count_dictionary = defaultdict(lambda: 0.0)
          total_french = defaultdict(lambda: 0.0)
          for english_sentence, french_sentence in self.data_reader.process_data(self.train_sentences, self.N):
              total_sentence_english = defaultdict(lambda: 0.0)

              for e_w in english_sentence:
                  for f_w in french_sentence:
                      total_sentence_english[e_w] += probability_dictionary[(e_w, f_w)]

              for e_w in english_sentence:
                  for f_w in french_sentence:
                      prob = probability_dictionary[(e_w, f_w)]
                      count = total_sentence_english[e_w]
                      value = prob / count
                      count_dictionary[(e_w, f_w)] += value
                      total_french[f_w] += value

          for word_alignment in self.word_alignments:
                  f_w = word_alignment[1]
                  probability_dictionary[word_alignment] = count_dictionary[word_alignment] / total_french[f_w]

          print("validating the sets")
          _, evaluate_val = self.validate_set(probability_dictionary,self.test_sentences,37)
          _, evaluate_train = self.validate_set(probability_dictionary,self.train_sentences,500)
          evaluations_val.append(evaluate_val)
          evaluations_train.append(evaluate_train)
      print("EM ended")
      #print(evaluations_train)
      #print(evaluations_val)
      return probability_dictionary, evaluations_train, evaluations_val

    def initialize_dictionary(self):
        init_prob = 1 / len(self.english_lex)
        return defaultdict(lambda: init_prob)


    def validate_set(self, probs_table, dataset,n_sentences):
        total_likelihood = 0
        translation = None
        for e_sentence, f_sentence in self.data_reader.process_data(dataset, n_sentences):
          e_length = len(e_sentence)
          f_length = len(f_sentence)
          translation = self.validate_sentence(e_sentence,f_sentence,probs_table)
          likelihood = self.compute_likelihood(translation, e_length, f_length)
          total_likelihood += likelihood
          #break
        translated_sentence = [e for ((e, _), _) in translation]
        return translation, total_likelihood / n_sentences

    def validate_sentence(self, english_sentence, french_sentence, probs_table):
        translation = []
        #print(french_sentence)
        for i, f_w in enumerate(french_sentence):
          #if i==1 or len(french_sentence)==1:
          #  continue
          word_alignments = []
          translated = None
          for e_w in english_sentence:
            if (e_w, f_w) in probs_table:
              word_alignments.append(((e_w, f_w),probs_table[(e_w, f_w)]))
              translated = self.translate_word(word_alignments)

            else:
              translated = ((None, f_w), probs_table[(None, f_w)])


          translation.append(translated)
        return translation

    def compute_likelihood(self, translation, e_length, f_length):
        log_sum = 0
        sentence_len = len(translation)
        for i in range(sentence_len):
          # because ((e_w,f_w), probability)
          translated_word = translation[i]
          log_sum += translated_word[1]
          #print(log_sum)
        tuple_prob = 1 / ((e_length + 1))
        log_sum = -np.log(tuple_prob*(log_sum + 1e-6))
        #print(log_sum)
        return log_sum

    def translate_word(self,word_alignments):
        return max(word_alignments, key = lambda x: x[1])


    def AER(self, dataset, probs_table):
      n_sentences = 37
      enlgish_indices = []
      translations = []
      sentence_alignments = []
      for e_sentence, f_sentence in self.data_reader.process_data(dataset, n_sentences):
        e_length = len(e_sentence)
        f_length = len(f_sentence)
        indices_e = list(range(e_length))
        translation = self.validate_sentence(e_sentence,f_sentence,probs_table)
        translated_sentence = [e for ((e, _), _) in translation]
        translations.append(translated_sentence)
        t_alignments = {}
        for i, e_w in enumerate(e_sentence):
          word_alignments = []
          for j, t_w in enumerate(translated_sentence):
            if e_w==t_w:
              word_alignments.append(j)
          t_alignments[i] = word_alignments
        sentence_alignments.append(t_alignments)



      print(sentence_alignments)
      return sentence_alignments




