import re
import time
import itertools
import sys

class DataReader(object):
  def __init__(self, filepaths,n_data, lexica = True):
      self.Data = self.read_data(filepaths)

  def read_data(self,data_files):
    data = []
    for file in data_files:
      connection = open(file)
      all_text = connection.read()
      connection.close()

      lines = all_text.splitlines()
      data.append(lines)

    return data

  def create_lexicon(self,language, n_data):
    lexicon = set()
    for sentence in language[:n_data]:
      lowered = sentence.lower()
      words = re.split(r'\W+', lowered)
      for word in words:
        lexicon.add(word)
    lexicon.add(None)
    lexicon = list(lexicon)
    lexicon = [word for word in lexicon if word != ""]
    return lexicon


  def process_data(self,Data, n_data):
    english = Data[0][:n_data]
    french = Data[1][:n_data]
    aligns = []
    for i in range(len(english)):
      english_sentence = english[i]
      french_sentence = french[i]
      lowered_e = english_sentence.lower()
      lowered_f = french_sentence.lower()
      #words_e = re.split(r'\W+', lowered_e)#[:-1]
      words_e = lowered_e.split(" ")#[:-1]
      words_e = [word for word in words_e if word != ""]
      #words_f = re.split(r'\W+', lowered_f)#[:-1]
      words_f = lowered_f.split(" ")
      words_f = [word for word in words_f if word != ""]
      words_e.insert(0,None)  #Null token fix
      aligns.append((words_e,words_f))
      #yield (words_e,words_f)
    return aligns
  def create_tuple(self, data):
    return list(itertools.product(data[0],data[1]))

  def create_alignments(self, Data, n_data):
      word_tuple_list = []

      for sentence_pair in self.process_data(Data, n_data):
          word_tuple_list += (self.create_tuple(sentence_pair))

      return list(set(word_tuple_list))
  def get_alignments(self, Data, n_data):
    return self.create_alignments(Data,n_data)


  def get_sentence_tuples(self):
      return self.sentence_tuples

  def get_sentences(self):
      return self.Data

  def get_french_lexicon(self, n_data):
      return self.create_lexicon(self.Data[0], n_data)

  def get_english_lexicon(self, n_data):
      return self.create_lexicon(self.Data[1], n_data)



