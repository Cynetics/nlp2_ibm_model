#Sandy is fatass
from IBM1 import *
from file_reader import *
from plot_losses import *
from aer import *
import time
import numpy as np
from nltk.translate import IBMModel1
from nltk.translate import IBMModel2

from nltk.translate import AlignedSent
from nltk.translate import Alignment


#start = time.time()
data_files = ["training/hansards.36.2.e","training/hansards.36.2.f"]
validation_files = ["testing/test/test.e","testing/test/test.f"]
N = 1000
#N = 231164
DR = DataReader(data_files, N)
val_DR = DataReader(validation_files,447, False)
validation_sentences = val_DR.get_sentences()
train_sentences = DR.get_sentences()

'''
alignations = DR.process_data(train_sentences,N)
alignations_val = DR.process_data(validation_sentences,447)
aligns_train = []
for e, f in alignations:
  aligns_train.append(AlignedSent(e,f))

for e, f in alignations_val:
  aligns_train.append(AlignedSent(e,f))

vals = aligns_train[-447:]
#print(tests.words)
IBM = IBMModel2(aligns_train,10)
IBM.train(aligns_train[:-447])

val_aligns = []
for sentence_pair in vals:
  val_align = list(sentence_pair.alignment)
  val_aligns.append(val_align)


with open("ibm2.mle.naacl", "w") as f:
  for i, sentence_alignment in enumerate(val_aligns):
    for (e_a, f_a) in sentence_alignment:
      if f_a == None:
        f.write(str(i+1)+" "+str(e_a+1)+" "+str(0)+"\n")
      else:
        f.write(str(i+1)+" "+str(e_a+1)+" "+str(f_a+1)+"\n")

#print(alignations[:5])
'''

french_lexicon = DR.get_french_lexicon(N)
english_lexicon = DR.get_english_lexicon(N)

IBM = IBM1(DR, train_sentences, validation_sentences, english_lexicon, french_lexicon,N)
print("Done with Data preprocessing and IBM model initialization")
#print("time: ", time.time() - start)
EM_probs, loss_train, loss_val = IBM.EM()
#print("time: ", time.time() - start)

A = IBM.AER(validation_sentences, EM_probs)
with open("ibm1.mle.naacl", "w") as f:
  for i, word in enumerate(A):
    for key, aligns in word.items():
      if len(aligns) == 0:
        f.write(str(i+1)+" "+str(key)+" "+str(0)+"\n")
      for value in aligns:
        f.write(str(i+1)+" "+str(key+1)+" "+str(value+1)+"\n")

plot_device = Plot(loss_train,loss_val)


plot_device.plot()
#===============================================================================
# AER
#===============================================================================
#test('validation/dev.wa.nonullalign', "output.wa.nonullalign")
