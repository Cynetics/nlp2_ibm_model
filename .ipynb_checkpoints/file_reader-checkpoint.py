import re

class DataReader(object):
  def __init__(self, filepaths):
      self.english, self.french = self.read_data(filepaths)
      self.english_vocabulary = self.process_data(self.english)
      self.french_vocabulary = self.process_data(self.french)

  def read_data(self,data_files):
      data = []
      for file in data_files:
          connection = open(file)
          data.append(connection.readlines())
          connection.close()
      return data

  def process_data(self,language):
    lexicon = set()
    for sentence in language:
      lowered = sentence.lower()
      words = re.split(r"(\w+[\w']*\w*\b)", lowered)
      for word in words:
        lexicon.add(word)
    lexicon = list(lexicon)
    return lexicon

  def get_vocabularies(self):
    return (self.english_vocabulary, self.french_vocabulary)
  
  def get_english(self):
      return self.english_vocabulary
  
  def get_french(self):
      return self.french_vocabulary

# data_files = ["training/hansards.36.2.e","training/hansards.36.2.f"]
# DR = DataReader(data_files)
# e, f = DR.get_vocabularies()
# print("English length: ", len(e), " French Length: ", len(f))
# print(e[:50])
# print(DR.english[:10])


