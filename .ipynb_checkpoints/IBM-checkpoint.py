import numpy as np
import itertools

import file_reader

class IBM_1(object):
    '''Takes a tuple of lexicons (foreign, english). Translation french -> english'''
    def __init__(self, f_lexicon, e_lexicon):
        self.translation_tuples = self.create_tuples(f_lexicon,e_lexicon)
#         self.translations_length = len(self.translation_tuples)
    
    def create_tuples(self,f_lexicon, e_lexicon):
        return list(itertools.product(f_lexicon,e_lexicon))
    
    def pr_initialization(self):
        pr_translations = {}
        for alignment in self.translation_tuples:
            probability_dictionary[alignment] = 1 / self.tuple_length
        return probability_dictionary
    
    def pr_esitmation(self,data, pr_translation):
        for sentence in data[0]:
            pass
            

data_files = ["training/hansards.36.2.e","training/hansards.36.2.f"]
DR = file_reader.DataReader(data_files)
e, f = DR.get_vocabularies()
print("English length: ", len(e), " French Length: ", len(f))
print(e[:50])
print(DR.english[:10])
            
model_1 = IBM_1(f,e)        